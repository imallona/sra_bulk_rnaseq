#!/bin/bash
#$ -S /bin/bash
#$ -e /imppc/labs/maplab/imallona/jobs/mjorda_plos_genetics-e.log
#$ -o /imppc/labs/maplab/imallona/jobs/mjorda_plos_genetics-o.log
#$ -N mjorda_plos_genetics
#$ -t 49-108
###$ -t 2-108
#$ -tc 2
#$ -pe smp 14
#$ -q imppcv3@curri14
#$ -V
#$ -M imallona@igtp.cat
#$ -m be

WD=~/thyroid
SOFT=/software/debian-8/
FEATURECOUNTS="$SOFT"/bio/subread-1.5.1/bin/featureCounts
TOPHAT2="$SOFT"/bio/tophat-2.1.0/bin/tophat2
NTHREADS=14
GTF=Homo_sapiens.GRCh38.90.gtf
GTF_EDITED="$IHOME"/data/ensembl90/hg38.edited.gtf
LOGGING="$IHOME"/thyroid_plos_genetics
TRANSCRIPTOME_DATA="$IHOME"/data/ensembl90/transcriptome_data/known

##

function download_ena {
    echo "$(date)" download start $1

    rm -rf "$1"
    mkdir -p $1

    fgrep $1 $2 | cut -f11 | cut -d";" -f1 > "$1"/url_1
    sed 's/_1/_2/g' "$1"/url_1 >  "$1"/url_2
    cat  "$1"/url_1 "$1"/url_2 > "$1"/urls
    cd $1
    wget -i  urls --no-verbose 
    cd ..
    
    echo "$(date)" download end $1
}

function run_tophat {
    echo "$(date)" tophat start $1
    
    $TOPHAT2  --output-dir $1 \
              --transcriptome-index="$TRANSCRIPTOME_DATA" \
              --num-threads $NTHREADS \
              --b2-very-fast \
              --no-coverage-search \
              --no-novel-juncs \
              /biodata/indices/species/Hsapiens/hg38 \
              "$1"/"$1"_1.fastq.gz \
              "$1"/"$1"_2.fastq.gz \
    &> "$1"/"$1"_tophat_overall.log
    
    rsync -av "$1"/*log "$LOGGING"/
    rsync -av "$1"/align_summary.txt "$LOGGING"/"$1"_align_summary.txt
    echo "$(date)" tophat end $1
}

function count {
    echo "$(date)" counting start $1

    "$FEATURECOUNTS" -T "$NTHREADS" \
                   -p -t exon \
                   -g gene_id \
                   -a "$GTF_EDITED" \
                   -o "$1"/"$1"_counts.txt \
                   "$1"/accepted_hits.bam
    
    rsync -av "$1"/*log "$LOGGING"/
    rsync -av "$1"/*counts* "$LOGGING"/

    echo "$(date)" counting end $1
}

function clean {
    echo "$(date)" cleaning start $1
    
    rm -rf "$WD"/"$1"
    
    echo "$(date)" cleaning end $1
}

##


mkdir -p $WD $LOGGING
cd $WD

ln -s $IHOME/src/sra_bulk_rnaseq/ena_ids

CURRENT=$(sed -n "${SGE_TASK_ID}"p ena_ids | cut -f6)

download_ena $CURRENT ena_ids

if [[ -f "$CURRENT"/"$CURRENT"_1.fastq.gz && -f "$CURRENT"/"$CURRENT"_1.fastq.gz ]]    
then
    run_tophat $CURRENT
    count $CURRENT
fi

clean $CURRENT

## final cleansing when last task
if [ "$SGE_TASK_ID" == "108" ]
then
    cd
    rm -rf $WD
fi
