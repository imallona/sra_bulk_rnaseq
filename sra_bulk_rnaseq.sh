#!/bin/bash
##
## bulk rnaseqs from ena accessions
##
## rather use the array_job job instead (this one builds the transcriptome etc)

WD=~/thyroid
SOFT=/software/debian-8/
FEATURECOUNTS="$SOFT"/bio/subread-1.5.1/bin/featureCounts
TOPHAT2="$SOFT"/bio/tophat-2.1.0/bin/tophat2
NTHREADS=28
GTF=Homo_sapiens.GRCh38.90.gtf
GTF_EDITED=hg38.edited.gtf
LOGGING="$IHOME"/thyroid
TRANSCRIPTOME_DATA="$IHOME"/data/ensembl90/transcriptome_data/known

mkdir -p $WD $LOGGING
cd $WD

wget ftp://ftp.ensembl.org/pub/release-90/gtf/homo_sapiens/Homo_sapiens.GRCh38.90.gtf.gz
gunzip Homo_sapiens.GRCh38.90.gtf.gz


echo $date building transcriptome index

awk '{print "chr"$0}' $GTF | sed 's/chrMT/chrM/g' > hg38.edited.gtf

$TOPHAT2 -G $GTF_EDITED \
         --num-threads $NTHREADS \
         --transcriptome-index=transcriptome_data/known \
         /biodata/indices/species/Hsapiens/hg38

rsync -av transcriptome_data/known "$TRANSCRIPTOME_DATA"
rsync -av hg38.edited.gtf "$IHOME"/data/ensembl90

# ln -s $IHOME/src/sra_bulk_rnaseq/ena_ids
 
# download_ena ERR1512530 ena_ids

# run_tophat ERR1512530

# count ERR1512530
